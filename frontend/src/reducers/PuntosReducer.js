import { GETPUNTOS, SETTEMPERATURA } from "../Constantes";

export const PuntosReducer = (state = {puntos:[]}, action)=>{
    switch(action.type)
    {
        case GETPUNTOS:
            return {puntos: action.payload};
        default:
            return state;
    }
};

export const TemperaturaReducer = (state= null, action)=>{
    switch(action.type)
    {
        case SETTEMPERATURA:
            return {...action.payload};
        default:
            return state;

    }
}

