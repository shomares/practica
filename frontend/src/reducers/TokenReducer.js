import { SETTOKEN } from "../Constantes";


export const TokenReducer = (state = {token: null}, action )=>{
    switch(action.type)
    {
        case SETTOKEN:
            return {token: action.payload};
        default:
            return state;
    }
};