import { SETGRAFICA, BORRARGRAFICA } from "../Constantes";

export const GraficaReducer = (state = {resultados:null}, action)=>{
    switch(action.type)
    {
        case SETGRAFICA:
            console.log(action);
            return {...action.payload};
        case BORRARGRAFICA:
            console.log(action);
            return {resultados:null};
        default:
            return state
    }
};