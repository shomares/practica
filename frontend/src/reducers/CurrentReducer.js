import { SETCURRENT, NUEVO, SETZONAS } from "../Constantes";


export const CurrentReducer = (state=null, action)=>{
    
    switch(action.type)
    {
        case SETCURRENT:
            console.log(action.payload);
            if(action.payload!==null)
            {
                return {...action.payload};
            }

            return null;
        case NUEVO:
            return {longitud: action.longitud, latitud: action.latitud, zona:"", venta:0, idZona:0, id:0, descripcion:""};
        default:
            return state;
    }
};

export const GetZonas = (state= [], action)=>{
    switch(action.type)
    {
        case SETZONAS:
            return [...action.payload];
        default:
            return state;
    }
}