export const SETTOKEN  = "SETTOKEN";
export const LIMPIARTOKEN  = "LIMPIARTOKEN";

export const GETPUNTOS  = "GETPUNTOS";
export const SETCURRENT  =  "SETCURRENT";
export const NUEVO  = "NUEVO";

export const SETTEMPERATURA  = "TEMPERATURA";

export const SETGRAFICA = "SETGRAFICA";
export const BORRARGRAFICA = "BORRARGRAFICA";

export const SETZONAS  = "SETZONAS";
 