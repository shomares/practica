import regeneratorRuntime, { async } from "regenerator-runtime";

import { SetMapa, SetZonas, SetGrafica } from "../actioncreators/MapaCreator";
import { Observable, interval } from "rxjs";

const api  = "http://localhost:7070/ws";

export  const  getToken = async () =>
{
    let value =  await fetch(`${api}/seguridad`, {
        credentials: 'include',
    });
    return await value.json();
};

const getPuntos = async (dispatch)=>{
    var puntos =  await fetch(`${api}/punto`, {
        credentials: 'include',
    });

    let value= await puntos.json();

    dispatch(SetMapa(value));
};

export const getMapa = () => async (dispatch) => {
    // dispatch actions, return Promise, etc.
    getPuntos(dispatch);
};



export const getZonas= () => async(dispatch)=>{
    // dispatch actions, return Promise, etc.
    var puntos =  await fetch(`${api}/zona`, {
        credentials: 'include',
    });

    let value= await puntos.json();

    dispatch(SetZonas(value));
};


export const getVentas= () => async(dispatch)=>{
    // dispatch actions, return Promise, etc.
    var puntos =  await fetch(`${api}/punto/venta`, {
        credentials: 'include',
    });

    let value= await puntos.json();

    dispatch(SetGrafica(value));
};


export const savePunto = (punto)=> async(dispatch)=>{
    await fetch(`${api}/punto`, {
        credentials: 'include',
        method : "POST",
        headers: {'Content-Type': 'application/json'},
        body : JSON.stringify(punto)
    });

    getPuntos(dispatch);
};

export const borrarPunto = (id)=> async(dispatch)=>{
    await fetch(`${api}/punto/${id}`, {
        credentials: 'include',
        method : "DELETE",
    });

    getPuntos(dispatch);
};

export const updatePunto = (id, punto)=> async(dispatch)=>{
    await fetch(`${api}/punto/${id}`, {
        credentials: 'include',
        method : "PUT",
        headers: {'Content-Type': 'application/json'},
        body : JSON.stringify(punto)
    });

    getPuntos(dispatch);
};

const getTemp = async()=> {
    let resp = await fetch(`${api}/temperatura`, {
        credentials: 'include'
    });
    let result = await resp.json();
    return result;
}

export const getTemperatura = ()=> {
    return new Observable(onnext=>{
        let inter = interval(100000);
        
        getTemp().then(r=>onnext.next(r));

        inter.subscribe(res=>{
            getTemp().then(r=>onnext.next(r));
        });
    });
};


