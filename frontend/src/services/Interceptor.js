export const GetInterceptor = (store)=>({
        request :(url, config)=>
        {
           let token = store.getState().TokenReducer.token;

           if(config != null)
           {
                let headers = {...config.headers, "XSSHeader": token };
                let nvoConfig = {...config, headers};
                return [url, nvoConfig];
           }
           else
           {
                let headers = { "XSSHeader": token };
                let nvoConfig = {headers};
          
                return [url, nvoConfig];
               
           }
        }
});