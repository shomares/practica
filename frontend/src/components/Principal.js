import  React from "react";
import  ReactDom from "react-dom";

import {connect} from "react-redux";
import { MapaComponent } from "./MapaComponent";
import {AgregarComponent} from "./Agregar";
import {TemperaturaComponent} from "./TemperaturaComponent";
import {GraficaComponent} from "./GraficaComponent";
import { getMapa, getZonas, getTemperatura } from "../services/RestServicio";
import {SetZonas, SetTemperatura} from "../actioncreators/MapaCreator";


class Principal extends React.Component
{
    componentDidMount()
    {
        let {onUpdate} = this.props;
        onUpdate();
    }

    render()
    {
        return (
            <div>
                <MapaComponent></MapaComponent>
                <AgregarComponent></AgregarComponent>
                <TemperaturaComponent></TemperaturaComponent>
                <GraficaComponent></GraficaComponent>
            </div>
        );
    }

}

const mapToDispach = (dispacher)=>(
    {
        onUpdate: ()=>{
            dispacher(getMapa());
            dispacher(getZonas());
            
            getTemperatura().subscribe(value=>{
                dispacher(SetTemperatura(value));
            })

        }
    }
)

export const PrincipalComponent = connect(null, mapToDispach)(Principal);