import React from "react";
import ReactDom from "react-dom";
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import {getVentas} from '../services/RestServicio';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {BorrarGrafica} from '../actioncreators/MapaCreator';
import {connect} from 'react-redux';

import "../styles/style.scss";

Exporting(Highcharts);

const createChart = (values=[], id)=>{
    console.log(id);
    Highcharts.chart(id, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Ventas por zona'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'ventas',
            colorByPoint: true,
            data: values.map(s=>{
                return{
                    name: s.nombre,
                    y: s.porcentaje ,
                    sliced: true
                }
            })
        }]
    });
};


class Grafica extends React.Component
{
    componentDidUpdate()
    {
        let {values} = this.props;
        console.log("gatos");
        console.log(values);
        if(values!==null)
        {
            createChart(values, "grafica");
        }
    }

    render()
    {
        let {values, onclose} = this.props;

        console.log(values);

        if(values!==null)
        {
        return (
                <div className="modal-wrapper" id="popup">
                    <div className="popup-contenedor">
                        <h2>Grafica</h2>
                        <div id="grafica"></div>
                        <a className="popup-cerrar" href="#" onClick={()=>{
                              onclose()

                        }}>Cerrar</a>
                    </div>
                </div>
            
        );
        }else
        {
            return (<div id="grafica" className="hidden"></div>)

        }

        
   
    }
}


const maptoState = (state)=>({
    values: state.GraficaReducer.resultados ,
});

const mapToDispach = (dispacher)=>({
        onclose: ()=>{
                console.log("a");
                dispacher(BorrarGrafica());
        }
    });

 const GraficaS = connect(maptoState, mapToDispach)(Grafica);

 const GraficaDialog = ({open, handleClose})=>(
    <div>
    <GraficaS></GraficaS>
    </div>
 )
 
 const maptoStateDialog = (state)=>({
    open: state.GraficaReducer.resultados!==null,
});

const maptodispacher = (dispacher)=>({
    
    handleClose: ()=>{
        console.log("error");
        dispacher(BorrarGrafica());
    }
});

export const GraficaComponent = connect(maptoStateDialog, maptodispacher)(GraficaDialog);