import React from "react";
import ReactDom from "react-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Button} from "@material-ui/core";
import {SetCurrent} from '../actioncreators/MapaCreator';
import {savePunto, updatePunto} from '../services/RestServicio';


let EdicionFormulario = ({handleSubmit, zonas})=>(
<form onSubmit={handleSubmit}>
      <div>
        <label>Descripcion</label>
        <div>
          <Field
            name="descripcion"
            component="input"
            type="text"
            placeholder="descripcion"
          />
        </div>
      </div>
      <div>
        <label>Total de Ventas</label>
        <div>
          <Field name="venta" component="input" type="number" placeholder="ventas" />
        </div>
      </div>
      <div>
        <label>Region</label>
        <div>
          <Field name="idZona" component="select">
            <option value="">Selecciona una zona...</option>
            {zonas.map(zona => (
              <option value={zona.idZona} key={zona.idZona}>
                {zona.nombre}
              </option>
            ))}
          </Field>
        </div>
      </div>
      <Button type="submit" color="primary" variant="contained" >
            Insertar
        </Button>
    </form>
);

EdicionFormulario = reduxForm({
    form: 'EdicionFormulario' // a unique identifier for this form
  })(EdicionFormulario)
  
  EdicionFormulario = connect(
    state => ({
      initialValues: state.CurrentReducer,
      zonas : state.GetZonas
    })
  )(EdicionFormulario)


const agregar = ({onSave, handleClose, open})=>(
    <Dialog open={open} onClose={handleClose}  aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Agregar editar</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Agregar Nuevo Punto
          </DialogContentText>
          <EdicionFormulario onSubmit={e=>{onSave(e)}}></EdicionFormulario>
        </DialogContent>
      </Dialog>

);

const mapToState = (state)=>({
    open: state.CurrentReducer!=null
});

const mapToDispach = (dispatch)=>({
    onSave: (e)=>{
        dispatch(SetCurrent(null));
        console.log(e);

        if(e.id===0)
        {
          dispatch(savePunto(e));
        }else
        {
          dispatch(updatePunto(e.id, e));
        }
    },
    handleClose: ()=>{
      dispatch(SetCurrent(null));
    }
});

export const AgregarComponent = connect(mapToState, mapToDispach)(agregar);




