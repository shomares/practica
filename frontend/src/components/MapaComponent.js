import React from "react";
import ReactDom from "react-dom";
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import {Button} from "@material-ui/core";
import {SetCurrent, SetNuevoPunto} from '../actioncreators/MapaCreator'
import {connect} from 'react-redux';
import {borrarPunto} from '../services/RestServicio';

const Informacion = ({posicion, onEditar, onBorrar})=>(
    <Popup>
        <h3>{posicion.descripcion}</h3>
        <div>
           <p>Cantidad de Ventas: {posicion.venta}</p>
           <p>Zona: {posicion.zona}</p>
        </div>
        <Button onClick={()=>{onEditar(posicion)}} variant="contained" color="primary" >Editar</Button>
        <Button onClick={()=>{onBorrar(posicion.id)}} variant="contained" color="secondary" >Borrar</Button>
    </Popup>
)

const Posicion = ({posicion, onEditar, onBorrar})=>(
    <Marker position={[posicion.longitud, posicion.latitud]}>
      <Informacion onEditar={onEditar} posicion={posicion} onBorrar={onBorrar} ></Informacion>
    </Marker>
);

const Posiciones = ({posiciones=[], onEditar, onBorrar={onBorrar}})=>{
    let pos =  posiciones.map(s=>(
            <Posicion key={s.id} onEditar={onEditar} posicion={s} onBorrar={onBorrar}></Posicion>
        ));
    return (<div>
        {pos}
    </div>);
}

let Mapa = ({position, posiciones=[], onEditar, OnClick, onBorrar})=>(
    <Map center={position} zoom={13} onClick={(e)=>OnClick(e)}>
    <TileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
    />
    <Posiciones posiciones={posiciones} onEditar={onEditar} onBorrar={onBorrar}></Posiciones>
  </Map>
);

const mapToState = (state)=>({
    posiciones: state.PuntosReducer.puntos,
    position: [19.4978, -99.1269]
});

const mapToDispach = (dispatch)=>({
    onEditar: (punto)=>{
        dispatch(SetCurrent(punto));
    },
    OnClick: (e)=>{
         dispatch(SetNuevoPunto(e.latlng.lat,e.latlng.lng ))
    },

    onBorrar: (id)=>{
        console.log(id);
        dispatch(borrarPunto(id));
    }

});

export const MapaComponent = connect(mapToState, mapToDispach) (Mapa);




