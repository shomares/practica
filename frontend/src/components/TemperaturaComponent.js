import React from "react";
import ReactDom from "react-dom";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {connect} from 'react-redux';
import {Button} from "@material-ui/core";
import {getVentas} from '../services/RestServicio';
import "../styles/style.scss";


const temperatura = ({temperatura, onconsulta})=>{
    console.log(temperatura);
    if(temperatura !== null && temperatura !== undefined)
    {
        return (<Card className={"temperatura"}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Temperatura actual:
          </Typography>
          <Typography variant="h5" component="h2">
           {temperatura.temperatura}
          </Typography>
          <Typography color="textSecondary">
            {temperatura.ciudad}
          </Typography>
          <Typography variant="body2" component="p">
              {temperatura.fecha}
          </Typography>
          <Button onClick={onconsulta}>Ver grafica</Button>
          </CardContent>
      </Card>
      )
    }else
    {
        return (<div></div>);
    }
    
};

const mapToState = (state)=>({
    temperatura: state.TemperaturaReducer
});

const maptoDispach = (dispatch)=>({
  onconsulta: ()=>{
    dispatch(getVentas());
  }
})

export const TemperaturaComponent = connect(mapToState,maptoDispach )(temperatura);