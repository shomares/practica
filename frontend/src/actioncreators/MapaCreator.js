import {GETPUNTOS, SETCURRENT, SETTOKEN, NUEVO, SETGRAFICA, SETZONAS, SETTEMPERATURA, BORRARGRAFICA} from "../Constantes";

export const SetMapa = (payload) =>({
    type: GETPUNTOS,
    payload
});

export const SetCurrent = (payload)=>({
    type: SETCURRENT,
    payload
});

export const SetToken = (payload)=>({
    type: SETTOKEN,
    payload
});

export const SetNuevoPunto = (longitud, latitud)=>({
    type: NUEVO,
    latitud,
    longitud
});


export const SetGrafica = (payload)=>({
    type : SETGRAFICA,
    payload
});

export const SetZonas = (payload)=>({
        type: SETZONAS,
        payload
});

export const  SetTemperatura = (payload)=>({
    type:SETTEMPERATURA,
    payload

});

export const  BorrarGrafica = ()=>({
    type:BORRARGRAFICA
});


