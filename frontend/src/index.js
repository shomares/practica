import  React from "react";
import  ReactDom from "react-dom";

import {CurrentReducer, GetZonas} from './reducers/CurrentReducer';
import {GraficaReducer} from './reducers/GraficaReducer';
import {PuntosReducer, TemperaturaReducer} from './reducers/PuntosReducer';
import {TokenReducer} from './reducers/TokenReducer';
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from 'redux-thunk';
import { getToken } from "./services/RestServicio";
import { SetToken } from "./actioncreators/MapaCreator";
import fetchIntercept from 'fetch-intercept';
import { GetInterceptor } from "./services/Interceptor";
import { PrincipalComponent } from "./components/Principal";
import { reducer as formReducer } from 'redux-form'

let reducers = combineReducers({CurrentReducer, GraficaReducer,PuntosReducer,  TokenReducer, GetZonas, TemperaturaReducer,  form: formReducer});
let store = createStore(reducers,  applyMiddleware(thunk));


fetchIntercept.register(GetInterceptor(store));


getToken().then(s=>{
    store.dispatch(SetToken(s.Value));
    ReactDom.render(
        <Provider store={store}>
            <PrincipalComponent></PrincipalComponent>
        </Provider>, 
    document.getElementById("app"));
});
