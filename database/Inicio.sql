If(db_id(N'EXAMEN') IS NULL)
    CREATE DATABASE EXAMEN
GO
use EXAMEN
GO

    IF NOT EXISTS (SELECT name from sys.tables where name = 'Punto')
    BEGIN
    CREATE TABLE [dbo].[Punto] (
        [Id] [bigint] NOT NULL IDENTITY,
        [Latitud] [real] NOT NULL,
        [Longitud] [real] NOT NULL,
        [Descripcion] [nvarchar](max),
        [Venta] [real] NOT NULL,
        [IdZona] [int] NOT NULL,
        CONSTRAINT [PK_dbo.Punto] PRIMARY KEY ([Id])
    )
        CREATE INDEX [IX_IdZona] ON [dbo].[Punto]([IdZona])

    END

    IF NOT EXISTS (SELECT name from sys.tables where name ='Zona')
    BEGIN
    CREATE TABLE [dbo].[Zona] (
        [IdZona] [int] NOT NULL IDENTITY,
        [Nombre] [nvarchar](max),
        CONSTRAINT [PK_dbo.Zona] PRIMARY KEY ([IdZona])
    )
    ALTER TABLE [dbo].[Punto] ADD CONSTRAINT [FK_dbo.Punto_dbo.Zona_IdZona] FOREIGN KEY ([IdZona]) REFERENCES [dbo].[Zona] ([IdZona]) ON DELETE CASCADE
END

