﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend
{
    using System;
    using System.Configuration;
    using System.ServiceProcess;
    using Src.Infraestructura;

    /// <summary>
    /// Inicio del sistema
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Inicia el programa
        /// </summary>
        /// <param name="args">Argumentos de inicio</param>
        public static void Main(string[] args)
        {
            ServiceBase[] services = 
            {
                new WebHost(ApiBackend.Properties.Settings.Default.Url)
            };

            if (Environment.UserInteractive)
            {
                Console.WriteLine("Iniciando los servicios");
                Console.WriteLine(System.Environment.GetEnvironmentVariable("PUNTOS_DBCONTEXT"));
                RunServices(services, args);
                Console.ReadLine();
                StopServices(services);
            }
            else
            {
                ServiceBase.Run(services);
            }
        }

        /// <summary>
        /// Corre los servicios de windows
        /// </summary>
        /// <param name="services">Lista de servicios</param>
        /// <param name="parameters">Lista de parametros</param>
        private static void RunServices(ServiceBase[] services, string[] parameters)
        {
            var type = typeof(ServiceBase);
            var method = type.GetMethod("OnStart", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            foreach (var service in services)
            {
                method.Invoke(service, new object[] { parameters });
            }
        }

        /// <summary>
        /// Termina los servicios
        /// </summary>
        /// <param name="services">Lista de servicios</param>
        private static void StopServices(ServiceBase[] services)
        {
            var type = typeof(ServiceBase);
            var method = type.GetMethod("OnStop", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            foreach (var service in services)
            {
                method.Invoke(service, null);
            }
        }
    }
}
