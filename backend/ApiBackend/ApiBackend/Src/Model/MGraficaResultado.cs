﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MGraficaResultado.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// Resultado de la grafica
    /// </summary>
    public class MGraficaResultado
    {
        /// <summary>
        /// Lista de resultados
        /// </summary>
        public IEnumerable<MGrafica> Resultados { get; set; }

        /// <summary>
        /// Total de ventas
        /// </summary>
        public float Total { get; set; }
    }
}
