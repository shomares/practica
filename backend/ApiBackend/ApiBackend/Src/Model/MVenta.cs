﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MVenta.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    /// <summary>
    /// Clase de venta
    /// </summary>
    public class MVenta
    {
        /// <summary>
        /// Suma de la venta
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Id de la zona
        /// </summary>
        public int IdZona { get; set; }
    }
}
