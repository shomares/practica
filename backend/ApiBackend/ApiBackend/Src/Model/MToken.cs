﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MToken.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    /// <summary>
    /// Token de cookie
    /// </summary>
    public class MToken
    {
        /// <summary>
        /// Valor de cookie
        /// </summary>
        public string Value { get; set; }
    }
}
