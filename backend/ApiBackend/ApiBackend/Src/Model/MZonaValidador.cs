﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MZonaValidador.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    using FluentValidation;
 
    /// <summary>
    /// Validador para zona
    /// </summary>
    public class MZonaValidador : AbstractValidator<MZona>
    {
        /// <summary>
        /// Crea una intancia de MZonaValidador
        /// </summary>
        public MZonaValidador()
        {
            this.RuleFor(r => r.Nombre).Matches(@"/^[A-Za-z0-9\s]+$/g");
        }
    }
}
