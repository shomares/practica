﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MGrafica.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    /// <summary>
    /// Clase que representa una grafica de pastel
    /// </summary>
    public class MGrafica
    {
        /// <summary>
        /// Porcentaje a partir del valor real
        /// </summary>
        public float Porcentaje { get; internal set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public string Nombre { get; internal set; }

        /// <summary>
        /// Valor real de la zona
        /// </summary>
        public float ValorReal { get; internal set; }
    }
}
