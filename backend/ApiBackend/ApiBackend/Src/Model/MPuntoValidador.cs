﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MPuntoValidador.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    using FluentValidation;

    /// <summary>
    /// Validador para punto
    /// </summary>
    public class MPuntoValidador : AbstractValidator<MPunto>
    {
        /// <summary>
        /// Crea una intancia de MPuntoValidador
        /// </summary>
        public MPuntoValidador()
        {
            this.RuleFor(r => r.Descripcion).Matches(@"/^[A-Za-z0-9\s]+$/g");
            this.RuleFor(r => r.Latitud).GreaterThan(0);
            this.RuleFor(r => r.Longitud).GreaterThan(0);
            this.RuleFor(r => r.Venta).GreaterThanOrEqualTo(0);
            this.RuleFor(r => r.Zona).Matches(@"/^[A-Za-z0-9\s]+$/g");
            this.RuleFor(r => r.Descripcion).MaximumLength(30);
            this.RuleFor(r => r.Zona).MaximumLength(30);
        }
    }
}
