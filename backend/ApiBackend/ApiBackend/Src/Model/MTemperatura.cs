﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MTemperatura.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ApiBackend.Src.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Clase DTO de temperatura
    /// </summary>
    public class MTemperatura
    {
        /// <summary>
        /// Temperatura actual del lugar
        /// </summary>
        public float Temperatura { get; set; }

        /// <summary>
        /// Latitud del lugar
        /// </summary>
        public float Pressure { get; set; }

        /// <summary>
        /// Longitud del lugar
        /// </summary>
        public float Humidity { get; set; }

        /// <summary>
        /// Ciudad del lugar
        /// </summary>
        public string Ciudad { get; set; }

        /// <summary>
        /// Fecha de temperatura
        /// </summary>
        public DateTime Fecha { get; internal set; }
    }
}
