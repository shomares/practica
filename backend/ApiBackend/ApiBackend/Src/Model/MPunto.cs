﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MPunto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FluentValidation;
    using FluentValidation.Attributes;

    /// <summary>
    /// Clase que representa el modelo de Punto de interes
    /// </summary>
    [Validator(typeof(MPuntoValidador))]
    public class MPunto
    {
        /// <summary>
        /// Identificador del punto
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Latitud del Punto
        /// </summary>
        public float Latitud { get; set; }

        /// <summary>
        /// Longitud del punto
        /// </summary>
        public float Longitud { get; set; }

        /// <summary>
        /// Descripcion del punto
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Venta del producto
        /// </summary>
        public float Venta { get; set; }
        
        /// <summary>
        /// Identificador de la Venta
        /// </summary>
        public int IdZona { get; set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public string Zona { get; set; }
    }
}
