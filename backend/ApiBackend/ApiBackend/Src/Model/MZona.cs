﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MZona.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Model
{
    using FluentValidation;
    using FluentValidation.Attributes;
 
    /// <summary>
    /// DTO para zona
    /// </summary>
    [Validator(typeof(MZonaValidador))]
    public class MZona
    {
        /// <summary>
        /// Llave de Zona
        /// </summary>
        public int IdZona { get; set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public string Nombre { get; set; }
    }
}
