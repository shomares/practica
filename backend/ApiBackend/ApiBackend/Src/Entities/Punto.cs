﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Punto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Entidad punto en base de datos
    /// </summary>
    [Table("Punto")]
    public class Punto
    {
        /// <summary>
        /// Identificador del punto
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Latitud del Punto
        /// </summary>
        public float Latitud { get; set; }

        /// <summary>
        /// Longitud del punto
        /// </summary>
        public float Longitud { get; set; }

        /// <summary>
        /// Descripcion del punto
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Venta del producto
        /// </summary>
        public float Venta { get; set; }

        /// <summary>
        /// Identificador de la Venta
        /// </summary>
        public int IdZona { get; set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public virtual Zona Zona { get; set; }
    }
}
