﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PuntoContext.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Linq;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Dao;

    /// <summary>
    /// Clase de acceso a datos EntityFramework
    /// </summary>
    public class PuntoContext : DbContext, ITransaccion
    {
        /// <summary>
        /// Current transaccion
        /// </summary>
        private DbContextTransaction transaccion;

        /// <summary>
        /// Crea una instancia de  PuntoContext
        /// </summary>
        /// <param name="conexion">Cadena de conexion</param>
        public PuntoContext(string conexion) : base(conexion)
        {
        }

        /// <summary>
        /// Crea una instancia de  PuntoContext
        /// </summary>
        public PuntoContext() : base("Migration")
        {
        }

        /// <summary>
        /// Tabla de base de datos puntos
        /// </summary>
        public virtual DbSet<Punto> Puntos { get; set; }

        /// <summary>
        /// Tabla de base de datos Zonas
        /// </summary>
        public virtual DbSet<Zona> Zonas { get; set; }

        /// <summary>
        /// Inicia una transaccion
        /// </summary>
        public void Begin()
        {
           this.transaccion = this.Database.BeginTransaction();
        }

        /// <summary>
        /// Commit la transaccion actual
        /// </summary>
        public void Commit()
        {
            if (this.transaccion != null)
            {
                this.transaccion.Commit();
            }
        }

        /// <summary>
        /// Regresa la transaccion
        /// </summary>
        public void Rollback()
        {
            if (this.transaccion != null)
            {
                this.transaccion.Rollback();
            }
        }
    }
}
