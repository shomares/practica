﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Zona.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Definicion de zona
    /// </summary>
    [Table("Zona")]
    public class Zona
    {
        /// <summary>
        /// Llave de Zona
        /// </summary>
        [Key]
        public int IdZona { get; set; }

        /// <summary>
        /// Nombre de la zona
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Relacion con punto
        /// </summary>
        public virtual ICollection<Punto> Puntos { get; set; }
    }
}
