﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HeaderSecurityOwin.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using log4net;
    using Microsoft.Owin;
    using Newtonsoft.Json;

    /// <summary>
    /// Hardcodeo de headers
    /// </summary>
    public class HeaderSecurityOwin : OwinMiddleware
    {
        /// <summary>
        /// Logger de errores
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Crea un instancia de HeaderSecurityOwin
        /// </summary>
        /// <param name="next">Siguiente middleware</param>
        public HeaderSecurityOwin(OwinMiddleware next) : base(next)
        {
        }

        /// <summary>
        /// Hardcodeo de headers de seguridad
        /// </summary>
        /// <param name="context">Contexto de owin</param>
        /// <returns>Un awaitable</returns>
        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                context.Response.OnSendingHeaders(
                  x =>
                  {
                      context.Response.Headers.Add("X-Frame-Options", new string[] { "SAMEORIGIN" });
                      context.Response.Headers.Add("X-XSS-Protection", new string[] { "1", "mode=block" });
                      context.Response.Headers.Add("X-Content-Type-Options", new string[] { "nosniff " });
                  },
                  null);
                await Next.Invoke(context);

                context.Response.Headers.Remove("Server");
            }
            catch (Exception ex)
            {
                this.HandleException(ex, context);
                throw;
            }
        }

        /// <summary>
        /// Handle excepcion
        /// </summary>
        /// <param name="ex">Error en owin</param>
        /// <param name="context">Contexto de owin</param>
        private void HandleException(Exception ex, IOwinContext context)
        {
            Logger.Error("Error grave", ex);
            var request = context.Request;
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ReasonPhrase = "Internal Server Error";
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(new { error = "Ha surgido un problema" }));
        }
    }
}
