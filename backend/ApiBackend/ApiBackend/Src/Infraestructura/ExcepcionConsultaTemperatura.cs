﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExcepcionConsultaTemperatura.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Excepcion de consulta de temperatura
    /// </summary>
    public class ExcepcionConsultaTemperatura : Exception
    {
        /// <summary>
        /// Crea una excepcion de temperatura
        /// </summary>
        /// <param name="message">Mesnaje de error</param>
        public ExcepcionConsultaTemperatura(string message) : base(message)
        {
        }
    }
}
