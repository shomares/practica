﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidattionAtribute.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    /// <summary>
    /// Atributo que valida el modelo de datos
    /// </summary>
    public class ValidattionAtribute : ActionFilterAttribute
    {
        /// <summary>
        /// Sobrescribe OnActionExecutingAsync
        /// </summary>
        /// <param name="actionContext">Peticion a la aplicacion</param>
        /// <param name="cancellationToken">Token de cancelacion</param>
        /// <returns>Un awaitable</returns>
        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            }

            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}
