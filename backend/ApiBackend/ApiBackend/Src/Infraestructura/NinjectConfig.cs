﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NinjectConfig.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Bussiness;
    using ApiBackend.Src.Bussiness.Impl;
    using ApiBackend.Src.Dao;
    using ApiBackend.Src.Dao.Impl;
    using ApiBackend.Src.Entities;
    using Ninject;

    /// <summary>
    /// Clase de configuracion para Ninject
    /// </summary>
    public class NinjectConfig
    {
        /// <summary>
        /// Singleton de NinjectConfig
        /// </summary>
        private static NinjectConfig instance;

        /// <summary>
        /// Intancia de Kernel de Ninject
        /// </summary>
        private readonly StandardKernel kernel;

        /// <summary>
        /// Crea una instancia de NinjectConfig
        /// </summary>
        private NinjectConfig()
        {
            this.kernel = new StandardKernel();
            this.InjectarDao();
            this.InjectarBussines();
        }

        /// <summary>
        /// Obtiene la instancia de NinjectConfig
        /// </summary>
        public static NinjectConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NinjectConfig();
                }

                return instance;
            }
        }

        /// <summary>
        /// Obtiene la instancia del kernel
        /// </summary>
        public StandardKernel Kernel
        {
            get
            {
                return this.kernel;
            }
        }

        /// <summary>
        /// Inyeccion de dao
        /// </summary>
        private void InjectarDao()
        {
            this.kernel.Bind<PuntoContext>().ToMethod(s => new PuntoContext(System.Environment.GetEnvironmentVariable("PUNTOS_DBCONTEXT")));
            this.kernel.Bind<ITransaccion>().ToMethod(s => s.Kernel.Get<PuntoContext>());
            this.kernel.Bind<IDaoPunto>().To<DaoPunto>();
            this.kernel.Bind<IDaoZona>().To<DaoZona>();
            this.kernel.Bind<IDaoTemperatura>().ToMethod(s => new DaoTemperatura(ApiBackend.Properties.Settings.Default.Clima, ApiBackend.Properties.Settings.Default.City, ApiBackend.Properties.Settings.Default.ApiKey)).WhenInjectedInto<DaoTemperaturaCache>();
            this.kernel.Bind<IDaoTemperatura>().To<DaoTemperaturaCache>().InSingletonScope();
        }

        /// <summary>
        /// Inyeccion de bussines
        /// </summary>
        private void InjectarBussines()
        {
            this.Kernel.Bind<IBussinesPunto>().To<BussinesPunto>().WhenInjectedInto<BussinesPuntoTransactional>();
            this.kernel.Bind<IBussinesPunto>().To<BussinesPuntoTransactional>();
        }
    }
}
