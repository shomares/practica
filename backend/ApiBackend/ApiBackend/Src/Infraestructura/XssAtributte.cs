﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XssAtributte.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using ApiBackend.Src.Controllers;

    /// <summary>
    /// Atributo para evitar XSS
    /// </summary>
    public class XssAtributte : ActionFilterAttribute
    {
        /// <summary>
        /// Sobrescribe OnActionExecutingAsync
        /// </summary>
        /// <param name="actionContext">Peticion de actionContext</param>
        /// <param name="cancellationToken">Cancelacion token</param>
        /// <returns>Un awaitable</returns>
        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var cookie = actionContext.Request.Headers.GetCookies(SeguridadController.Cookie).FirstOrDefault();
            var header = actionContext.Request.Headers.GetValues(SeguridadController.Header).FirstOrDefault();

            if (cookie != null)
            {
                var str = cookie[SeguridadController.Cookie].Value;

                if (!str.Equals(header))
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "XSS");
                }
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "XSS");
            }

            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}
