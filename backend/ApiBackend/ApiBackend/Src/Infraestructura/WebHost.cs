﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebHost.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Infraestructura
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Filters;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.Owin.Hosting;
    using Newtonsoft.Json.Serialization;
    using Ninject.Web.Common.OwinHost;
    using Ninject.Web.WebApi.OwinHost;
    using Owin;
    using Swashbuckle.Application;

    /// <summary>
    /// Hebhost de OWIN
    /// </summary>
    public class WebHost : ServiceBase
    {
        /// <summary>
        /// Url del Webhosting
        /// </summary>
        private readonly string url;

        /// <summary>
        /// Instancia de OWIN
        /// </summary>
        private IDisposable webServer;

        /// <summary>
        /// Crea una instancia de WebHost
        /// </summary>
        /// <param name="url">Url del webhosting</param>
        public WebHost(string url)
        {
            this.url = url;
        }

        /// <summary>
        /// Inicia el servicio de selhosting
        /// </summary>
        /// <param name="args">Parametros de entrada</param>
        protected override void OnStart(string[] args)
        {
            this.webServer = WebApp.Start(
                this.url,
                app =>
                {
                    HttpConfiguration config = new HttpConfiguration();
                    config.MapHttpAttributeRoutes();
                    config.Filters.Add(new ValidattionAtribute());
                    config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
                    config.EnableSwagger(c =>
                    {
                        var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                        var commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                        var commentsFile = Path.Combine(baseDirectory, commentsFileName);

                        c.SingleApiVersion("v1", "Puntos de interes")
                          .Description("Practica")
                          .Contact(cc => cc
                          .Name("Eneas Mejia")
                          .Email("shomares@gmail.com"));
                      
                        c.IncludeXmlComments(commentsFile);
                    }).EnableSwaggerUi();

                    app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

                    app.UseStaticFiles("/dist");
                    app.Use(typeof(HeaderSecurityOwin));
                    app.UseNinjectMiddleware(() => NinjectConfig.Instance.Kernel).UseNinjectWebApi(config);
                   
                });

            base.OnStart(args);
        }

        /// <summary>
        /// Para el servicio de selfhosting
        /// </summary>
        protected override void OnStop()
        {
            this.webServer.Dispose();
            base.OnStop();
        }
    }
}
