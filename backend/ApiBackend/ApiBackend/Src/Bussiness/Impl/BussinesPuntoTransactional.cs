﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BussinesPuntoTransactional.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Bussiness.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Dao;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Clase que implementa IBussinesPunto de modo transaccional
    /// </summary>
    public class BussinesPuntoTransactional : IBussinesPunto
    {
        /// <summary>
        /// Intancia de itransaccion
        /// </summary>
        private readonly ITransaccion transaccion;

        /// <summary>
        /// Intancia de IBussinesPunto
        /// </summary>
        private readonly IBussinesPunto bussinesPunto;

        /// <summary>
        /// Crea una instancia de BussinesPuntoTransactional
        /// </summary>
        /// <param name="transaccion">Instancia de ITransaccion</param>
        /// <param name="bussinesPunto">Instancia de IBussinesPunto</param>
        public BussinesPuntoTransactional(ITransaccion transaccion, IBussinesPunto bussinesPunto)
        {
            this.transaccion = transaccion;
            this.bussinesPunto = bussinesPunto;
        }

        /// <summary>
        /// Consulta ventas de forma transaccional
        /// </summary>
        /// <returns>Los datos de la grafica de pastel</returns>
        public async Task<MGraficaResultado> ConsultarVentas()
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.ConsultarVentas();
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }

        /// <summary>
        /// Borra un punto de forma transaccional
        /// </summary>
        /// <param name="id">Id a borrar</param>
        /// <returns>True si es correcto, false sino</returns>
        public async Task<bool> Delete(long id)
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.Delete(id);
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }

        /// <summary>
        /// Obtiene todas los puntos
        /// </summary>
        /// <returns>Lista de puntos</returns>
        public async Task<IEnumerable<MPunto>> GetAll()
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.GetAll();
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }

        /// <summary>
        /// Obtiene un elemento por id
        /// </summary>
        /// <param name="id">Id a buscar</param>
        /// <returns>Un elemento</returns>
        public async Task<MPunto> GetById(long id)
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.GetById(id);
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }

        /// <summary>
        /// Guarda un elemento
        /// </summary>
        /// <param name="elemento">Elemento a guardar</param>
        /// <returns>El punto con identificador</returns>
        public async Task<MPunto> Save(MPunto elemento)
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.Save(elemento);
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }

        /// <summary>
        /// Actualiza un punto
        /// </summary>
        /// <param name="punto">Punto a buscar</param>
        /// <param name="id">Id a buscar</param>
        /// <returns>El punto actualizado</returns>
        public async Task<MPunto> Update(MPunto punto, long id)
        {
            try
            {
                this.transaccion.Begin();
                var result = await this.bussinesPunto.Update(punto, id);
                this.transaccion.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaccion.Rollback();
                throw;
            }
            finally
            {
                this.transaccion.Dispose();
            }
        }
    }
}
