﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BussinesPunto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Bussiness.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Dao;
    using ApiBackend.Src.Entities;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Clase que implementa BussinesPunto
    /// </summary>
    public class BussinesPunto : IBussinesPunto
    {
        /// <summary>
        /// Implementacion de IDaoPunto
        /// </summary>
        private readonly IDaoPunto daoPunto;

        /// <summary>
        /// Implementacion de IDaoZona
        /// </summary>
        private readonly IDaoZona daoZona;

        /// <summary>
        /// Crea una instancia de BussinesPunto
        /// </summary>
        /// <param name="daoPunto">Implemntacion de daoPunto</param>
        /// <param name="daoZona">Implemntacion de daoZona</param>
        public BussinesPunto(IDaoPunto daoPunto, IDaoZona daoZona)
        {
            this.daoPunto = daoPunto;
            this.daoZona = daoZona;
        }

        /// <summary>
        /// Consulta las ventas
        /// </summary>
        /// <returns>Una instancia de MGrafica</returns>
        public async Task<MGraficaResultado> ConsultarVentas()
        {
            IList<MGrafica> result = new List<MGrafica>();
            var sumaVentas = await this.daoPunto.CountVentas();
            var ventas = await this.daoPunto.GetVentas();

            Parallel.ForEach(
                ventas,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, 
                element => 
                {
                    var value = (element.Value / sumaVentas) * 100;
                    lock (this)
                    {
                        result.Add(new MGrafica
                        {
                            Porcentaje = value,
                            Nombre = element.Nombre,
                            ValorReal = element.Value
                        });
                    }
                });

            if (result.Count > 0)
            {
                return new MGraficaResultado
                {
                    Resultados = result.ToArray(),
                    Total = sumaVentas
                };
            }

            return null;
        }

        /// <summary>
        /// Borra un punto
        /// </summary>
        /// <param name="id">Id a buscar</param>
        /// <returns>Un true o false</returns>
        public async Task<bool> Delete(long id)
        {
            return await this.daoPunto.Delete(id);
        }

        /// <summary>
        /// Obtiene una lista de todos los puntos
        /// </summary>
        /// <returns>Lista de puntos</returns>
        public async Task<IEnumerable<MPunto>> GetAll()
        {
            return await this.daoPunto.GetAll();
        }

        /// <summary>
        /// Obtiene un punto por id
        /// </summary>
        /// <param name="id">Id a buscar</param>
        /// <returns>Una lista de punto</returns>
        public async Task<MPunto> GetById(long id)
        {
           return await this.daoPunto.GetById(id);
        }

        /// <summary>
        /// Guarda un punto
        /// </summary>
        /// <param name="elemento">Elemento a guardar</param>
        /// <returns>El elemento con su identificador</returns>
        public async Task<MPunto> Save(MPunto elemento)
        {
            var zona = await this.daoZona.GetById(elemento.IdZona);

            if (zona == null)
            {
                var zonanva = await this.daoZona.Save(new MZona { Nombre = elemento.Zona });
                elemento.IdZona = zonanva.IdZona;
            }

            return await this.daoPunto.Save(elemento);
        }

        /// <summary>
        /// Actualiza un punto
        /// </summary>
        /// <param name="punto">Punto a buscar</param>
        /// <param name="id">Id a buscar</param>
        /// <returns>El punto actualizado</returns>
        public async Task<MPunto> Update(MPunto punto, long id)
        {
            return await this.daoPunto.Update(punto, id);
        }
    }
}
