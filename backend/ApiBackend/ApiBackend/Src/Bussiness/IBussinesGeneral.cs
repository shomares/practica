﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBussinesGeneral.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Bussiness
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Define las reglas de todos los catalogos
    /// </summary>
    /// <typeparam name="T">Tipo de la entidad</typeparam>
    /// <typeparam name="K">Tipo de la llave de la entidad</typeparam>
    public interface IBussinesGeneral<T, K>
    {
        /// <summary>
        /// Obtiene una lista de elementos
        /// </summary>
        /// <returns>Un lista de elementos en un awaitable</returns>
        Task<IEnumerable<T>> GetAll();

        /// <summary>
        /// Obtiene un elemento por id
        /// </summary>
        /// <param name="id">El id del elemento</param>
        /// <returns>Una instancia de el elemento</returns>
        Task<T> GetById(K id);

        /// <summary>
        /// Guarda el elemento 
        /// </summary>
        /// <param name="elemento">Elemento a guardar</param>
        /// <returns>El elemento con su identificador</returns>
        Task<T> Save(T elemento);

        /// <summary>
        /// Borra un punto
        /// </summary>
        /// <param name="id">Elemento a eliminar</param>
        /// <returns>True si se borro correctamente</returns>
        Task<bool> Delete(K id);

        /// <summary>
        /// Actualiza un elemento
        /// </summary>
        /// <param name="punto">Elemento nuevo</param>
        /// <param name="id">Llave a actualizar</param>
        /// <returns>El Elemento actualizado</returns>
        Task<T> Update(T punto, K id);
    }
}
