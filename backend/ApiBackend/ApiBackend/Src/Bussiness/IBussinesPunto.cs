﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBussinesPunto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Bussiness
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Src.Model;

    /// <summary>
    /// Interfaz que define las operaciones de los puntos de interes
    /// </summary>
    public interface IBussinesPunto : IBussinesGeneral<MPunto, long>
    {
        /// <summary>
        /// Consulta ventas del punto
        /// </summary>
        /// <returns>Una instancia de grafica</returns>
        Task<MGraficaResultado> ConsultarVentas();
    }
}
