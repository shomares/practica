﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemperaturaController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using ApiBackend.Src.Dao;
    using ApiBackend.Src.Infraestructura;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Temperatura Controler
    /// </summary>
    [RoutePrefix("ws")]
    [XssAtributte]
    public class TemperaturaController : ApiController
    {
        /// <summary>
        /// Impementacion de IDaoTemperatura
        /// </summary>
        private readonly IDaoTemperatura daoTemperatura;

        /// <summary>
        /// Crea una instancia de TemperaturaController
        /// </summary>
        /// <param name="daoTemperatura">Implementacion de daotemperatura</param>
        public TemperaturaController(IDaoTemperatura daoTemperatura)
        {
            this.daoTemperatura = daoTemperatura;
        }

        /// <summary>
        /// Consulta de temperatura de la ciudad de mexico
        /// </summary>
        /// <returns>Temperatura actual</returns>
        [Route("temperatura")]
        public async Task<IHttpActionResult> Get()
        {
            var value = await this.daoTemperatura.GetTemperatura();
            if (value != null)
            {
                return this.Ok<MTemperatura>(value);
            }

            return this.NotFound();
        }
    }
}
