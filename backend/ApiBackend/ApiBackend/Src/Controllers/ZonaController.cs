﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ZonaController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using ApiBackend.Src.Dao;
    using ApiBackend.Src.Infraestructura;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Controlador para zona
    /// </summary>
    [RoutePrefix("ws")]
    [XssAtributte]
   public class ZonaController : ApiController
    {
        /// <summary>
        /// Implementacion de IDaoZona
        /// </summary>
        private readonly IDaoZona daoZona;

        /// <summary>
        /// Crea una intancia de ZonaController
        /// </summary>
        /// <param name="daoZona">Implmentacion de IDaoZona</param>
        public ZonaController(IDaoZona daoZona)
        {
            this.daoZona = daoZona;
        }

        /// <summary>
        /// Consulta todas las zonas
        /// </summary>
        /// <returns>Lista de zonas</returns>
        [HttpGet]
        [Route("zona")]
        public async Task<IHttpActionResult> Get()
        {
            var result = await this.daoZona.GetAll();

            if (result != null)
            {
                return this.Ok<IEnumerable<MZona>>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Guarda las zonas
        /// </summary>
        /// <param name="zona">Zona a guardar</param>
        /// <returns>El identificador de la zona</returns>
        [HttpPost]
        [Route("zona")]
        public async Task<IHttpActionResult> Save(MZona zona)
        {
            var result = await this.daoZona.Save(zona);

            if (result != null)
            {
                return this.Ok<MZona>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Borra una zona
        /// </summary>
        /// <param name="idZona">Identificador de la zona</param>
        /// <returns>Ok si la borra correctamente</returns>
        [HttpDelete]
        [Route("zona/{idZona}")]
        public async Task<IHttpActionResult> Borrar(int idZona)
        {
            var result = await this.daoZona.Delete(idZona);

            if (result)
            {
                return this.Ok<bool>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Actualiza una zona
        /// </summary>
        /// <param name="idZona">Zona a cambair</param>
        /// <param name="zona">Zona nueva</param>
        /// <returns>Identificador de la zona</returns>
        [HttpPut]
        [Route("zona/{idZona}")]

        public async Task<IHttpActionResult> Update(int idZona, MZona zona)
        {
            var result = await this.daoZona.Update(zona, idZona);

            if (result != null)
            {
                return this.Ok<MZona>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Libera los recursos
        /// </summary>
        /// <param name="disposing">Patron disposable</param>
        protected override void Dispose(bool disposing)
        {
            this.daoZona.Dispose();
            base.Dispose(disposing);
        }
    }
}
