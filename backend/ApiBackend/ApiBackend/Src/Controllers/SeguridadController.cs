﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeguridadController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using ApiBackend.Src.Model;
    using Newtonsoft.Json;

    /// <summary>
    /// Controlador que genera la cookie para evitar xss
    /// </summary>
    [RoutePrefix("ws")]
    public class SeguridadController : ApiController
    {
        /// <summary>
        /// Nombre de la cookie
        /// </summary>
        public const string Cookie = "XSSCookie";

        /// <summary>
        /// Header obligatorio
        /// </summary>
        public const string Header = "XSSHeader";

        /// <summary>
        /// Obtiene el cookie y el valor del token
        /// </summary>
        /// <returns>La cookie y el valor en el header</returns>
        [HttpGet]
        [Route("seguridad")]
        public HttpResponseMessage Get()
        {
            var resp = new HttpResponseMessage();
            var token = new MToken
            {
                Value = Guid.NewGuid().ToString()
            };
       
            var cookie = new CookieHeaderValue(Cookie, token.Value);
            cookie.Expires = DateTimeOffset.Now.AddDays(1);
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";
            cookie.HttpOnly = true; 
            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
            resp.Content = new StringContent(JsonConvert.SerializeObject(token));
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("json/application");

            return resp;
        }
    }
}
