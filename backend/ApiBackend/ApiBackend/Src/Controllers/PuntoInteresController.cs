﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PuntoInteresController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using ApiBackend.Src.Infraestructura;
    using ApiBackend.Src.Model;
    using Bussiness;

    /// <summary>
    /// Controlador para punto de Interes
    /// </summary>
    [RoutePrefix("ws")]
    [XssAtributte]
    public class PuntoInteresController : ApiController
    {
        /// <summary>
        /// Intancia de bussinesPunto
        /// </summary>
        private readonly IBussinesPunto bussinesPunto;

        /// <summary>
        /// Crea una instancia de PuntoInteresController
        /// </summary>
        /// <param name="bussinesPunto">Instancia de bussinesPunto</param>
        public PuntoInteresController(IBussinesPunto bussinesPunto)
        {
            this.bussinesPunto = bussinesPunto;
        }

        /// <summary>
        /// Guarda el punto de interes
        /// </summary>
        /// <param name="punto">Punto de interes a guardar</param>
        /// <returns>El identificador del punto de interes</returns>
        [HttpPost]
        [Route("punto")]
        public async Task<IHttpActionResult> Guardar(MPunto punto)
        {
            var result = await this.bussinesPunto.Save(punto);

            if (result != null)
            {
                return this.Ok<MPunto>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Obtiene todos los puntos de interes
        /// </summary>
        /// <returns>Una lista de puntos</returns>
        [HttpGet]
        [Route("punto")]
        public async Task<IHttpActionResult> GetAll()
        {
            var result = await this.bussinesPunto.GetAll();

            if (result != null)
            {
                return this.Ok<IEnumerable<MPunto>>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Consultar por id
        /// </summary>
        /// <param name="id">Id a consultar</param>
        /// <returns>Elemento a consultar</returns>
        [HttpGet]
        [Route("punto/{id}")]
        public async Task<IHttpActionResult> GetById(long id)
        {
            var result = await this.bussinesPunto.GetById(id);

            if (result != null)
            {
                return this.Ok<MPunto>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Borra el punto de interes
        /// </summary>
        /// <param name="id">Id de punto de interes</param>
        /// <returns>True si es correcto</returns>
        [HttpDelete]
        [Route("punto/{id}")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            var result = await this.bussinesPunto.Delete(id);

            if (result)
            {
                return this.Ok<bool>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Actualiza un punto de interes
        /// </summary>
        /// <param name="id">Id a buscar</param>
        /// <param name="punto">Punto a buscar</param>
        /// <returns>El punto actualizado</returns>
        [HttpPut]
        [Route("punto/{id}")]
        public async Task<IHttpActionResult> Update(long id, MPunto punto)
        {
            var result = await this.bussinesPunto.Update(punto, id);

            if (result != null)
            {
                return this.Ok<MPunto>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Consulta las ventas
        /// </summary>
        /// <returns>Objeto de ventas</returns>
        [HttpGet]
        [Route("punto/venta")]
        public async Task<IHttpActionResult> ConsultarVentas()
        {
            var result = await this.bussinesPunto.ConsultarVentas();

            if (result != null)
            {
                return this.Ok<MGraficaResultado>(result);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Metodo para probar si el servicio response
        /// </summary>
        /// <returns>200 si es correcto</returns>
        [Route("punto/health")]
        [HttpGet]
        public IHttpActionResult Health()
        {
            return this.Ok();
        }
    }
}
