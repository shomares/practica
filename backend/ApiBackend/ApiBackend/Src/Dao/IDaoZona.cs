﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDaoZona.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Bussiness;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Interfaz para zona
    /// </summary>
    public interface IDaoZona : IBussinesGeneral<MZona, long>, IDisposable
    {
    }
}
