﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDaoTemperatura.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Clase de acceso a Web Rest de temperatura
    /// </summary>
    public interface IDaoTemperatura
    {
        /// <summary>
        /// Obtiene la temperatura del un servicio de 
        /// </summary>
        /// <returns>Temperatura actual</returns>
        Task<MTemperatura> GetTemperatura();
    }
}
