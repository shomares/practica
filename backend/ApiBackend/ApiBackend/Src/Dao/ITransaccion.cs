﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransaccion.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao
{
    using System;

    /// <summary>
    /// Definicion para ITransaccion
    /// </summary>
    public interface ITransaccion : IDisposable
    {
        /// <summary>
        /// Inicia una transaccion
        /// </summary>
        void Begin();

        /// <summary>
        /// Cancela una transaccion
        /// </summary>
        void Rollback();

        /// <summary>
        /// Aplica cambios
        /// </summary>
        void Commit();
    }
}
