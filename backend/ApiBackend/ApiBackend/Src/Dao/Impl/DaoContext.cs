﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DaoContext.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao.Impl
{
    using ApiBackend.Src.Entities;

    /// <summary>
    /// Clase de DaoContext
    /// </summary>
    public class DaoContext
    {
        /// <summary>
        /// Instancia de PuntoContext
        /// </summary>
        protected readonly PuntoContext PuntoContext;

        /// <summary>
        /// Crea una instancia de DaoPunto
        /// </summary>
        /// <param name="puntoContext">Conexion a base de datos</param>
        public DaoContext(PuntoContext puntoContext)
        {
            this.PuntoContext = puntoContext;
        }
    }
}
