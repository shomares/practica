﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DaoTemperatura.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Infraestructura;
    using ApiBackend.Src.Model;
    using Newtonsoft.Json;

    /// <summary>
    /// Clase que implementa DaoTemperatura
    /// </summary>
    public class DaoTemperatura : IDaoTemperatura
    {
        /// <summary>
        /// Ruta del servicio rest
        /// </summary>
        private readonly string url;

        /// <summary>
        /// Cuidad de consulta
        /// </summary>
        private readonly string city;

        /// <summary>
        /// Api key
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// Crea una instancia de DaoTemperatura
        /// </summary>
        /// <param name="url">Url del servicio</param>
        /// <param name="city">Ciudad de consulta servicio</param>
        /// <param name="apiKey">Api key</param>
        public DaoTemperatura(string url, string city, string apiKey)
        {
            this.url = url;
            this.city = city;
            this.apiKey = apiKey;
        }

        /// <summary>
        /// Implementacion de IDaoTemperatura con servicio REST
        /// </summary>
        /// <returns>Temperatura actual</returns>
        public async Task<MTemperatura> GetTemperatura()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(this.url);

            var result = await client.GetAsync($"data/2.5/weather?q={this.city}&appid={this.apiKey}&units=metric");

            if (result.IsSuccessStatusCode)
            {
                string value = await result.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject<dynamic>(value);

                return new MTemperatura
                {
                    Temperatura = obj.main.temp,
                    Pressure = obj.main.pressure,
                    Humidity = obj.main.humidity,
                    Fecha = DateTime.Now
                };
            }

            throw new ExcepcionConsultaTemperatura($"Error no se consulto correctamente la temperatura {result.StatusCode.ToString()}");
        }
    }
}
