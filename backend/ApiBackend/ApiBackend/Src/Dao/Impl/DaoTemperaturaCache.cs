﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DaoTemperaturaCache.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Implementacion de DaoTemperaturaCache
    /// </summary>
    public class DaoTemperaturaCache : IDaoTemperatura
    {
        /// <summary>
        /// Dao Temperatura
        /// </summary>
        private readonly IDaoTemperatura daoTemperatura;

        /// <summary>
        /// Temperatura de cache
        /// </summary>
        private MTemperatura temperatura;

        /// <summary>
        /// Crea instancia de DaoTemperaturaCache
        /// </summary>
        /// <param name="daoTemperatura">Implementacion de IDaoTemperatura</param>
        public DaoTemperaturaCache(IDaoTemperatura daoTemperatura)
        {
            this.daoTemperatura = daoTemperatura;
        }

        /// <summary>
        /// Obtiene la temperatura con cache
        /// </summary>
        /// <returns>La temperatura</returns>
        public async Task<MTemperatura> GetTemperatura()
        {
            DateTime now = DateTime.Now;
            MTemperatura temp = null;

            if (this.temperatura != null)
            {
                if ((this.temperatura.Fecha - now).TotalMinutes > 10)
                {
                    temp = await this.daoTemperatura.GetTemperatura();

                    lock (this)
                    {
                        this.temperatura = temp;
                    }
                }
            }
            else
            {
                temp = await this.daoTemperatura.GetTemperatura();
                lock (this)
                {
                    this.temperatura = temp;
                }
            }

            return new MTemperatura
            {
                Ciudad = this.temperatura.Ciudad,
                Fecha = this.temperatura.Fecha,
                Humidity = this.temperatura.Humidity,
                Temperatura = this.temperatura.Temperatura,
                Pressure = this.temperatura.Pressure
            };
        }
    }
}
