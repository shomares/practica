﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DaoZona.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Entities;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Implementacion de IDaoZona
    /// </summary>
    public class DaoZona : DaoContext,  IDaoZona
    {
        /// <summary>
        /// Crea una instancia de DaoZona
        /// </summary>
        /// <param name="puntoContext">Conexion a base de datos</param>
        public DaoZona(PuntoContext puntoContext) : base(puntoContext)
        {
        }

        /// <summary>
        /// Borra un azona
        /// </summary>
        /// <param name="id">Zona a borrar</param>
        /// <returns>True si se borra correctamente</returns>
        public async Task<bool> Delete(long id)
        {
            var value = await this.PuntoContext.Zonas.FindAsync(id);
            if (value != null)
            {
                this.PuntoContext.Zonas.Remove(value);
                await this.PuntoContext.SaveChangesAsync();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Elimina recursos
        /// </summary>
        public void Dispose()
        {
            this.PuntoContext.Dispose();
        }

        /// <summary>
        /// Obtiene todas las zonas
        /// </summary>
        /// <returns>Lista de zonas</returns>
        public async Task<IEnumerable<MZona>> GetAll()
        {
            return await this.PuntoContext.Zonas.Select(r => new MZona
            {
                IdZona = r.IdZona,
                Nombre = r.Nombre
            }).ToArrayAsync();
        }

        /// <summary>
        /// Obtiene un elemento por id
        /// </summary>
        /// <param name="id">Elemento a buscar</param>
        /// <returns>Una zona</returns>
        public async Task<MZona> GetById(long id)
        {
            var value = await this.PuntoContext.Zonas.FindAsync(id);
            if (value != null)
            {
                return new MZona
                {
                    IdZona = value.IdZona,
                    Nombre = value.Nombre
                };
            }

            return null;
        }

        /// <summary>
        /// Guarda una zona
        /// </summary>
        /// <param name="elemento">Elemento a guardar</param>
        /// <returns>El identificador de la zona</returns>
        public async Task<MZona> Save(MZona elemento)
        {
            Zona zona = new Zona
            {
                Nombre = elemento.Nombre
            };

            this.PuntoContext.Zonas.Add(zona);
            await this.PuntoContext.SaveChangesAsync();

            return new MZona
            {
                IdZona = zona.IdZona
            };
        }

        /// <summary>
        /// Actualiza una zona
        /// </summary>
        /// <param name="punto">Punto a actualizar</param>
        /// <param name="id">Id de zona</param>
        /// <returns>Una zona</returns>
        public async Task<MZona> Update(MZona punto, long id)
        {
            var value = await this.PuntoContext.Zonas.FindAsync(id);
            if (value != null)
            {
                return new MZona
                {
                    IdZona = value.IdZona,
                    Nombre = value.Nombre
                };
            }

            return null;
        }
    }
}
