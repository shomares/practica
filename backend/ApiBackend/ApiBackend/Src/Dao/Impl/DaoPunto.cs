﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DaoPunto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Entities;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Clase que implementa DaoPunto
    /// </summary>
    public class DaoPunto : DaoContext, IDaoPunto
    {
        /// <summary>
        /// Crea una instancia de DaoPunto
        /// </summary>
        /// <param name="puntoContext">Conexion a base de datos</param>
        public DaoPunto(PuntoContext puntoContext) : base(puntoContext) 
        {
        }

        /// <summary>
        /// Suma el total de ventas
        /// </summary>
        /// <returns>La suma de las ventas</returns>
        public async Task<float> CountVentas()
        {
            return await this.PuntoContext.Puntos.SumAsync(s => s.Venta);
        }

        /// <summary>
        /// Borra un punto por id
        /// </summary>
        /// <param name="id">Elemnto a borrar</param>
        /// <returns>True si lo borra</returns>
        public async Task<bool> Delete(long id)
        {
            var value = await this.PuntoContext.Puntos.FindAsync(id);

            if (value != null)
            {
                this.PuntoContext.Puntos.Remove(value);
                await this.PuntoContext.SaveChangesAsync();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Consulta todos los puntos
        /// </summary>
        /// <returns>Una lista de puntos</returns>
        public async Task<IEnumerable<MPunto>> GetAll()
        {
            return await this.PuntoContext.Puntos.Select(r => new MPunto
            {
                Descripcion = r.Descripcion,
                Id = r.Id,
                IdZona = r.IdZona,
                Latitud = r.Latitud,
                Longitud = r.Longitud,
                Zona = r.Zona.Nombre,
                Venta = r.Venta
            }).ToArrayAsync();
        }

        /// <summary>
        /// Obtiene un punto por id
        /// </summary>
        /// <param name="id">Elemento a consultar</param>
        /// <returns>El punto</returns>
        public async Task<MPunto> GetById(long id)
        {
            var value = await this.PuntoContext.Puntos.FindAsync(id);

            if (value != null)
            {
                return new MPunto
                {
                    Descripcion = value.Descripcion,
                    Id = value.Id,
                    IdZona = value.IdZona,
                    Latitud = value.Latitud,
                    Longitud = value.Longitud,
                    Zona = value.Zona.Nombre,
                    Venta = value.Venta
                };
            }

            return null;
        }

        /// <summary>
        /// Consulta todas las ventas
        /// </summary>
        /// <returns>Una lista de ventas</returns>
        public async Task<IEnumerable<MVenta>> GetVentas()
        {
            return await this.PuntoContext.Puntos.GroupBy(r => new { r.IdZona, r.Zona.Nombre }).Select(r => new MVenta
            {
                IdZona = r.Key.IdZona,
                Nombre = r.Key.Nombre,
                Value = r.Sum(s => s.Venta)
            }).ToArrayAsync();
        }

        /// <summary>
        /// Guarda un punto
        /// </summary>
        /// <param name="elemento">Punto a guardar</param>
        /// <returns>Un punto</returns>
        public async Task<MPunto> Save(MPunto elemento)
        {
            var punto = new Punto
            {
                Descripcion = elemento.Descripcion,
                IdZona = elemento.IdZona,
                Latitud = elemento.Latitud,
                Longitud = elemento.Longitud,
                Venta = elemento.Venta
            };

            this.PuntoContext.Puntos.Add(punto);

            await this.PuntoContext.SaveChangesAsync();

            return new MPunto
            {
                Id = punto.Id
            };
        }

        /// <summary>
        /// Actualiza un punto
        /// </summary>
        /// <param name="punto">Punto a actualizar</param>
        /// <param name="id">Id a agregar</param>
        /// <returns>El punto actualizado</returns>
        public async Task<MPunto> Update(MPunto punto, long id)
        {
            var value = await this.PuntoContext.Puntos.FindAsync(id);

            if (value != null)
            {
                value.Descripcion = punto.Descripcion;
                value.Venta = punto.Venta;

                await this.PuntoContext.SaveChangesAsync();

                return new MPunto
                {
                    Id = value.Id
                };
            }

            return null;
        }
    }
}
