﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDaoPunto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ApiBackend.Src.Dao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ApiBackend.Src.Bussiness;
    using ApiBackend.Src.Entities;
    using ApiBackend.Src.Model;

    /// <summary>
    /// Interfaz que define el acceso a datos para puntos de interes
    /// </summary>
    public interface IDaoPunto : IBussinesGeneral<MPunto, long>
    {
        /// <summary>
        /// Cuenta los elementos guardados de punto
        /// </summary>
        /// <returns>Suma de las ventas</returns>
        Task<float> CountVentas();

        /// <summary>
        /// Obtiene una lista de ventas
        /// </summary>
        /// <returns>Lista de ventas agrupadas</returns>
        Task<IEnumerable<MVenta>> GetVentas();
    }
}
