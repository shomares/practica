function buildVS
{
    param
    (
        [parameter(Mandatory=$true)]
        [String] $path,

        [parameter(Mandatory=$false)]
        [bool] $nuget = $true,
        
        [parameter(Mandatory=$false)]
        [bool] $clean = $true
    )
    process
    {
        $msBuildExe = 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\msbuild.exe'

        if ($nuget) {
            Write-Host "Restoring NuGet packages" -foregroundcolor green
            nuget restore "$($path)"
        }

        if ($clean) {
            Write-Host "Cleaning $($path)" -foregroundcolor green
            & "$($msBuildExe)" "$($path)" /t:Clean /p:Configuration=Release /p:Platform="Any CPU" /m
        }

        Write-Host "Building $($path)" -foregroundcolor green
        & "$($msBuildExe)" "$($path)" /t:Build /p:Configuration=Release /p:Platform="Any CPU" /m
    }
}


cd frontend

Write-Host "Install NPM $($path)" -foregroundcolor green
& npm install

Write-Host "Build React $($path)" -foregroundcolor green
& npx webpack

Write-Host "Copy React $($path)" -foregroundcolor green
Copy-Item -Path dist -Filter "*.*" -Recurse -Destination ../backend/ApiBackend/ApiBackend -Container -force

cd ..
Write-Host "Build  C#  $($path)" -foregroundcolor green
buildVS -path .\backend\ApiBackend\ApiBackend.sln -nuget $false -clean $true

Write-Host "Publish React $($path)" -foregroundcolor green
Copy-Item -Path .\backend\ApiBackend\ApiBackend\dist -Filter "*.*" -Recurse -Destination .\backend\ApiBackend\ApiBackend\bin\Release -Container -force

cd database
Write-Host "Install database $($path)" -foregroundcolor green
& sqlcmd -S "192.168.99.100" -U "sa" -P "yourStrong(!)Password" -i "inicio.sql"

Write-Host "Install datos $($path)" -foregroundcolor green
& sqlcmd -S "192.168.99.100" -U "sa" -P "yourStrong(!)Password" -i "datos.sql"

cd ..

